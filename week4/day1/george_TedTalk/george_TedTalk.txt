	The thesis of this presentation was about 2/3 to 3/4 of the way through 
when David McCandless talked about information design.  He argued that a 
rich, beautiful graphic could give the mind a break from density of text or 
spoken information -- like an oasis bringing life and refreshment in a desert 
of information overload.  He supported this claim by showing a graphic 
representing the human senses by information bandwidth where eyesight 
dominated the graph.

	The first figure McCandless presented was my favorite because it presented
insight and perspective on hard-to-fathom quantities.  It showed rectangles 
representing amounts spent on conflicts, charitable endeavors, and business.

The "Mountains Out of Molehills" figure was my least favorite because it
had a weird z-axis that didn't have meaning.  I suppose McCandless did this
to seperate the plots on the x-axis, but I didn't find it appealing.  I would
have plotted several plots with common x-axis instead.