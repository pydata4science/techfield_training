Logistic regression

gensim library

P(x) / (1-P(x))

Maximum Likeelihood Estimation
	called "Backpropogation" in deep learning

Newton's method (when derivative is smooth)

Stochastic GD is faster & cheaper to compute than Batch GD

sigmoid function used to implement logistic regression with regularization

.map, .filter, .reduce functional programing in Python

