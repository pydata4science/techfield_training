

def letter_counter(letter_string):
	'''
	Manipulates a string of letters such that repeated letters are replaced with the
	number of repeated letters preceding the repeated letter in the returned string.
	'''
	count = 1
	return_string = ''
	for letter_string_index in range(len(letter_string)):
		if letter_string_index < len(letter_string) - 1:
			if letter_string[letter_string_index] == letter_string[letter_string_index + 1]:
				count += 1
			else:
				if count == 1:
					return_string += letter_string[letter_string_index]
				else:
					new_string = str(count) + letter_string[letter_string_index]
					return_string += new_string
					count = 1
		else:
			if count == 1:
					return_string += letter_string[letter_string_index]
			else:
				new_string2 = str(count) + letter_string[letter_string_index]
				return_string += new_string2
	return return_string

print(letter_counter('aabbbnsa') == '2a3bnsa')
print(letter_counter('aabbbnsaa') == '2a3bns2a')
print(letter_counter('a') == 'a')
print(letter_counter('') == '')
print(letter_counter('ab') == 'ab')
print(letter_counter('aa') == '2a')